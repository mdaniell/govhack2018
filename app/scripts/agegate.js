const Cookies = require('js-cookie');

module.exports = AgeGate = (function () {

  var ageGateCookie;
  var debug;
   //debug = true; // remove commenting to debug

  function setCookie() {
    Cookies.set('ageGate', 1, { expires: 365 }); // expires after 365 days
  }


  function init() {

    $('body').on('click','#ageYes', function(e){
      e.preventDefault();
      setCookie();
      $('html').removeClass('age-gate-show');
      //$('#video').get(0).play(); // hidden until video live
    });

    $('body').on('click','#ageNo', function(e){
      e.preventDefault();
      $('.age-gate-error').addClass('no-entry');
    });

    ageGateCookie = Cookies.get('ageGate');
    if (ageGateCookie == 1 && debug) {
      Cookies.remove('ageGate');
    }
    else if (ageGateCookie == 1) {
      //alert('Cookie already set');
      $('html').removeClass('age-gate-show');
      //$('#video').get(0).play(); // hidden until video live
    }
    else {
      $('html').addClass('age-gate-show');
    }
  }

  return {
    init: init,
    setCookie: setCookie
  };
} ());
